var gameManager;
function setup() {
    createCanvas(windowWidth, windowHeight);
    bg = loadImage('assets/bg.jpeg');
    gameManager = new GameManager();
}

function draw() {
    background(bg);
    gameManager.render();
}

function keyPressed() {
    if (keyCode === 32) {
        gameManager.jumpPlayer();
    }
}

