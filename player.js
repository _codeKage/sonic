class Player {
    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.vel = createVector();
        this.img = loadImage('assets/player.gif');
        this.gravity = 0.2;
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x, this.location.y, 100, 100);
        pop();
    }

    canJump() {
        return this.location.y === (windowHeight / 1.25);
    }

    update() {
      this.vel.y += this.gravity;
      this.location.y += this.vel.y;
      this.location.y = constrain(this.location.y, 0, windowHeight / 1.25);
    }

}