class GameManager {

    constructor() {
        this.player = new Player(createVector(windowWidth / 11, windowHeight / 1.25), 5, 20);
        this.enemies = [];
        this.score = 0;
        this.gameSpeed = 5;
    }

    render() {
        this.displayPlayer();
        this.generateEnemy();
        this.displayEnemies();
        this.displayScore();
    }

    displayPlayer(){
        this.player.update();
        this.player.display();
    }

    jumpPlayer() {
        if (this.player.canJump()) {
            this.player.vel.y = -7;
        }
    }

    generateEnemy() {
        if(frameCount % 120 === 0) {
            this.enemies.push(new Enemy(
                   createVector(windowWidth + 5, windowHeight / 1.25),
                   100,
                   100
               ));
        }
    }


    displayScore() {
        textSize(30);
        fill(color(0, 0, 0));
        text("Score: " +Math.round(this.score), windowWidth / 2.12 , windowHeight / 8);
    }

    displayEnemies() {
        this.enemies.forEach((enemy, index) => {
            if (enemy.onEdges()) {
                this.enemies.splice(index, 1);
                this.score++;
            } else if (enemy.collided(this.player.height - 30, this.player.location)) {
                alert('You lose');
                this.score = 0;
                this.enemies.splice(index, 1);
                location.reload();
            } else {
                try {
                    this.enemies[index].update(this.gameSpeed);
                    this.enemies[index].display();
                } catch(exception) {

                }
            }
        });
    }

}