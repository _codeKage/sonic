class Enemy {

    constructor(location, height, width) {
        this.location = location;
        this.height = height;
        this.width = width;
        this.imgBase = Math.round(Math.random(0 ,3));
        this.path = this.imgBase ? 'assets/enemy.gif' : 'assets/another_enemy.gif';
        this.img = loadImage(this.path);
    }

    update(gameSpeed) {
        this.location.x -= gameSpeed;
    }

    display() {
        push();
        imageMode(CENTER);
        image(this.img, this.location.x, this.location.y, this.width, this.height);
        pop();
    }

    onEdges(){
        return this.location.x < 0;
    }

    collided(side, loc){
        return (side + this.height) > this.location.dist(loc);
    }

}